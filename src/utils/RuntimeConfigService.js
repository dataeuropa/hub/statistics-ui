/**
 * Vue.js plugin to consume configurations using environment variables
 */

import merge from 'merge-anything';
// The configuration object that contains symbols for overwriting during runtime
import config from '../../config/runtime-config.js';
import { baseConfig } from '../../config/user-config.js';
export const injectionKey = Symbol('runtimeConfig');

// Takes a base configuration (e.g., process.env) and a runtime configuration
// and merges the runtime configuration over the base configuration to overwrite it.
// Overwritten values are always of type string. Pass an empty string to model a falsy value.
const RuntimeConfiguration = {
  install(app, options = {}) {
    const defaultOptions = {
      debug: true,
      baseConfig
    }
    const opts = Object.assign({}, defaultOptions, options)
    // Custom merge rule to ignore values that start with $VUE_APP_
    // i.e., use this.$env property when environment variable is not set
    const ignoreUnusedVariables = (originVal, newVal) => {
      const result = newVal
      // Evaluate Boolean Types (Fix Issue https://gitlab.fokus.fraunhofer.de/viaduct/organisation/issues/592)
      if (newVal === 'false') return false
      if (newVal === 'true') return true
      // Take originVal when env variable is not set
      if (originVal !== undefined && typeof newVal === 'string') {
        // Environment variable not set (e.g., development env)
        if (newVal.startsWith('$VITE_')) {
          return originVal
        }
      }
      // Use the new value
      return result
    }

    const mergeOptions = {
      extensions: [ignoreUnusedVariables]
    }

    const merged = clean(merge(mergeOptions, opts.baseConfig, config))
    if (opts.debug) {
      console.log(`Runtime configuration = ${JSON.stringify(merged, null, 2)}`); // eslint-disable-line
    }

    app.config.globalProperties.$env = merged; // eslint-disable-line
    app.provide(injectionKey, merged);
  }
}

/**
 * Removes all elements from the config that have a non-replaced runtime-config.js key
 * @param o
 * @param current
 */
function clean(o, current = {}) {
  Object.keys(o).forEach(key => {
    const value = o[key];
    if (value?.constructor === Object) {
      current[key] = clean(value);
    } else if (typeof value === 'string' && ! value.startsWith("$VITE_")) {
      current[key] = value;
    } else if (typeof value !== 'string') {
      current[key] = value;
    }
  });
  return current;
}

export default RuntimeConfiguration;
