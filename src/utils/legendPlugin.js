const onClick = function (e, index, chart) {
  const canvasId = chart.canvas.id;
  const textId = index => `line-${canvasId}-clickable-legend-text-${index}`;

  const alreadyHidden = (chart.getDatasetMeta(index).hidden === null) ? false : chart.getDatasetMeta(index).hidden;
  let anyOthersAlreadyHidden = false;
  let allOthersHidden = true;

  // figure out the current state of the labels
  chart.data.datasets.forEach(function (e, i) {
      const meta = chart.getDatasetMeta(i);
      if (i !== index) {
          if (meta.hidden) {
              anyOthersAlreadyHidden = true;
          } else {
              allOthersHidden = false;
          }
      }
  });
  // if the label we clicked is already hidden
  // then we now want to unhide (with any others already unhidden)
  if (alreadyHidden) {
      chart.getDatasetMeta(index).hidden = null;
      document.getElementById(textId(index)).classList.remove("hidden");
  } else {
    // otherwise, lets figure out how to toggle visibility based upon the current state
    chart.data.datasets.forEach(function (e, i) {
      const element = document.getElementById(textId(i));
      const meta = chart.getDatasetMeta(i);
      element.classList.add("hidden");
      if (i !== index) {
        // handles logic when we click on visible hidden label and there is currently at least
        // one other label that is visible and at least one other label already hidden
        // (we want to keep those already hidden still hidden)
        if (anyOthersAlreadyHidden && !allOthersHidden) {
          element.classList.add("hidden");
          meta.hidden = true;
        } else {
          // toggle visibility
          meta.hidden = meta.hidden === null ? true : null;
          if (meta.hidden == null){
              element.classList.remove("hidden");
          }
        }
      } else {
        meta.hidden = null;
        element.classList.remove("hidden");
      }
    });
  }
  chart.update();
}

export const customLegend = {
  id: 'customLegend',
  beforeInit: function(chart, args, options) {
    const canvasId = chart.canvas.id;
    // if (canvasId === "chart-country") {
      const legendElement = document.getElementById(options.elementId);
      if ( ! legendElement) {
        console.error(`GraphJS custom legend: HTML element #${options.elementId} not found!`);
        return;
      }
      if (legendElement && legendElement.childNodes.length === 0) {
        const ul = document.createElement('ul');
        ul.style.display = "flex";
        ul.style.flexFlow = "wrap";
        ul.style.columnGap = "40px";
        ul.style.padding = "0";
        ul.innerHTML = "";
        chart.data.datasets.forEach((dataset, i) => {
          const hide = options.hideAbove && i >= options.hideAbove;
          const hiddenClass = hide? 'class="hidden"' : "";
          ul.innerHTML += `
          <li class="line-${canvasId}-clickable-legend" style="margin-bottom:8px">
            <span style="width: 40px;height:15px;margin-right:5px;background-color: ${ dataset.backgroundColor }"></span>
            <span ${hiddenClass} id="line-${canvasId}-clickable-legend-text-${i}">${ dataset.label }</span>
          </li>
        `;
          if (options.hideAbove && i >= options.hideAbove) {
            chart.getDatasetMeta(i).hidden = true;
          }
        });
        setTimeout(() => {
          const legendItems = [
            ...document.querySelectorAll(`.line-${canvasId}-clickable-legend`)
          ];
          legendItems.forEach((item, i) => {
            item.addEventListener("click", (e) => {
              onClick(e, i, chart)
            });
          });
        });
        return legendElement.appendChild(ul);
      }
    // }
  }
};
