import { createApp } from 'vue';
import { createHead } from '@unhead/vue';
import RuntimeConfiguration from './utils/RuntimeConfigService.js';
import App from './App.vue';
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import Sticky from 'vue-sticky-directive'
import i18njson from './i18n/lang.js'
import { createI18n } from 'vue-i18n';
import deuFooterHeader, { Header, Footer } from '@deu/deu-header-footer'
import UniversalPiwik from '@piveau/piveau-universal-piwik'

// Import Font Awesome Icons Library for vue
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faGoogle,
  faGooglePlus,
  faGooglePlusG,
  faFacebook,
  faFacebookF,
  faInstagram,
  faTwitter,
  faLinkedinIn
} from '@fortawesome/free-brands-svg-icons'
import {
  faComment,
  faExternalLinkAlt,
  faPlus,
  faMinus,
  faArrowDown,
  faArrowUp,
  faInfoCircle
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

const app = createApp(App);

import 'bootstrap';
import '../node_modules/bootstrap/scss/bootstrap.scss';
import './styles/styles.scss';
import '@deu/deu-header-footer/dist/deu-header-footer.css';
import '@ecl/preset-ec/dist/styles/ecl-ec.css';

/**********************************************
 *  Integrating the EC component library here *
 **********************************************/
import '@ecl/preset-ec/dist/styles/ecl-ec.css';
import './assets/img/data-europa-logo.svg';

const head = createHead();
app.use(head);

app.config.productionTip = false

library.add(faGoogle, faGooglePlus, faGooglePlusG, faFacebook, faFacebookF, faInstagram, faTwitter, faLinkedinIn, faComment, faExternalLinkAlt, faPlus, faMinus, faArrowDown, faArrowUp, faInfoCircle)
app.component('font-awesome-icon', FontAwesomeIcon)

app.use(Sticky)
app.use(RuntimeConfiguration, { debug: false })

// app.use(deuFooterHeader)
app.component('deu-header', Header)
app.component('deu-footer', Footer)

app.use(router);

app.use(UniversalPiwik, {
  router,
  isPiwikPro: app.config.globalProperties.$env.TRACKER_IS_PIWIK_PRO,
  trackerUrl: app.config.globalProperties.$env.TRACKER_TRACKER_URL,
  siteId: app.config.globalProperties.$env.TRACKER_SITE_ID,
  debug: app.config.globalProperties.$env.NODE_ENV === 'development'
})

const i18n = createI18n({
  locale: 'en', // set locale
  messages: i18njson, // set locale messages
  silentTranslationWarn: true
});
app.use(i18n);
app.use(deuFooterHeader, { i18n });
app.config.globalProperties.i18n = i18n;

app.mount('#app');
