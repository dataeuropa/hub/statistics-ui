import path from 'path';
import vue from '@vitejs/plugin-vue';

const isProduction = process.env.NODE_ENV === 'production' && ! (process.env.BUILD_MODE === 'test');
const base = isProduction ? '/catalogue-statistics' : '/';

export default {
  base,
  plugins: [vue()],
  resolve: {
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src'),
      }
    ]
  },
  build: {
    rollupOptions: {
      output: {
        entryFileNames: 'app.[hash].js',
      }
    }
  }
};
