# Catalogue Statistics UI

## Setup

Clone the repository:
```
git clone ...
```
Navigate to the new directory:
```
cd piveau-hub-statistics-ui
```

Install all needed packages:
```
npm install
```

Run the development server:
```
npm run dev
```

After this, the application will be running on http://localhost:8080.


## Dependencies and their licenses

***
This [3rd-party-licenses.csv](3rd-party-licenses.csv) contains a list of the dependencies and their licenses.
