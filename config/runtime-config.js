/**
 * Configuration template file to bind specific properties to environment variables.
 * All values must have the prefix $VITE_.
 * Their corresponding environment variable key labels must be the values without the $ character.
 * This object should be structurally identical (name and path) to the standard configuration file.
 */
export default {
  ROOT_API: '$VITE_ROOT_API',
  MATOMO_URL: '$VITE_MATOMO_URL',
  PIWIK_ID: '$VITE_PIWIK_ID',
  TRACKER_IS_PIWIK_PRO: '$VITE_TRACKER_IS_PIWIK_PRO',
  TRACKER_TRACKER_URL: '$VITE_TRACKER_TRACKER_URL',
  TRACKER_SITE_ID: '$VITE_TRACKER_SITE_ID',
  SHOW_SPARQL: '$VITE_SHOW_SPARQL',
}
